import Vue from "vue";
import App from "./App.vue";
//
import router from "./router";
// fuzzy-search library:
import VueFuse from 'vue-fuse'
// http requests:
import VueResource from "vue-resource";
//
import BootstrapVue from 'bootstrap-vue'
import '../public/bootstrap/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
//
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHatWizard, faDice, faBookOpen, faEye, faListUl, faDiceOne, faDiceTwo, faDiceThree, faDiceFour, faDiceFive, faDiceSix, faDizzy, faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
//
library.add(faHatWizard, faDice, faBookOpen, faEye, faListUl, faDiceOne, faDiceTwo, faDiceThree, faDiceFour, faDiceFive, faDiceSix, faDizzy, faStar)
Vue.component('font-awesome-icon', FontAwesomeIcon)
//
Vue.config.productionTip = false;
//
Vue.use(VueFuse, VueResource, BootstrapVue)
//
//
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
