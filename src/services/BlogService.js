import Api from '@/services/Api'
//
//
// BLOG SERVICE
//
//
export default {
    // GET ALL POSTS
    fetchPosts() {
        // ce PATH correspond à celui de l'api
        return Api().get('posts')
    },
    // GET POST BY ID
    fetchPost(id) {
        return Api().get('/posts/' + id)
    }
}