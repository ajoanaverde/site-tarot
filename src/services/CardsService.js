import Api from '@/services/Api'
//
//
// CARDS SERVICE
//
//
export default {
    // GET ALL CARDS
    fetchCards() {
        // ce PATH correspond à celui de l'api
        return Api().get('cards')
    },
    // GET CARD BY ID
    fetchCard(id) {
        return Api().get('cards/' + id)
    },
    // GET CARD BY TITLE
    findByName(name) {
        return Api().get('cards?name=' + name)
    },
    // TIRAGE RANDOM
    tirageRandom() {
        return Api().get('tirage')
    }
}