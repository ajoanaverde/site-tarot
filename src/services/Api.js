import axios from "axios";

export default () => {
    return axios.create({
        // API URL
        baseURL: 'http://localhost:3000'
    })

}