//
import Vue from "vue";
import VueRouter from "vue-router";
//
// View components
import Home from "../views/Home.vue";
import Cards from "../views/cards/Cards.vue";
import SingleCard from "../views/cards/SingleCard.vue";
import Blog from "../views/blog/Blog.vue";
import SinglePost from "../views/blog/SinglePost.vue";
import VotreTirage from "../views/VotreTirage";
import Tirage from "../views/TirageRandom";
import Dice from "../views/Dice"
// 404
import NotFound from "../views/NotFound";

Vue.use(VueRouter);

const routes = [
  {
    // Landing Page
    path: "/",
    name: "Home",
    component: Home
  },
  {
    // List of all cards
    path: "/cards",
    name: "CardsMain",
    component: Cards
  },
  {
    // Single Card detail
    path: "/cards/:id",
    name: "SingleCard",
    component: SingleCard,
    // children: [
    //   path:,
    //   component:
    // ]
  },
  {
    // List of all Posts
    path: "/blog",
    name: "Blog",
    component: Blog
  },
  {
    // Single Post detail
    path: "/post/:id",
    name: "Single Post",
    component: SinglePost,
  },
  {
    // Tirage personalisé
    // work in progress
    path: "/votretirage",
    name: "Votre Tirage",
    component: VotreTirage,
  },
  {
    // Tirage Random
    // Feature principal
    // work in progress
    path: "/tirage",
    name: "Tirage",
    component: Tirage
  },
  {
    path: "/de",
    name: "Magic Dice",
    component: Dice
  },
  {
    // 404
    path: "*",
    component: NotFound
  }
];

// Router instaciacion
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});


// Router export
export default router;
